<?php
return array(
	"siteUrl"=>"http://127.0.0.1/ubi-test/",
	"database"=>array(
			"type"=>"mysql",
			"dbName"=>"scrumpoker",
			"serverName"=>"127.0.0.1",
			"port"=>3306,
			"user"=>"root",
			"password"=>"",
			"options"=>array(),
			"cache"=>false
			),
	"sessionName"=>"ubi-test",
	"namespaces"=>array(),
	"templateEngine"=>"Ubiquity\\views\\engine\\Twig",
	"templateEngineOptions"=>array(
			"cache"=>false
			),
	"test"=>false,
	"debug"=>true,
	"logger"=>function (){return new \Ubiquity\log\libraries\UMonolog("ubi-test",\Monolog\Logger::INFO);},
	"di"=>array(
			"jquery"=>function ($controller){
							$jquery=new \Ajax\php\ubiquity\JsUtils(["defer"=>true],$controller);
							$jquery->semantic(new \Ajax\Semantic());
							return $jquery;
						}
			),
	"cache"=>array(
			"directory"=>"cache/",
			"system"=>"Ubiquity\\cache\\system\\ArrayCache",
			"params"=>array()
			),
	"mvcNS"=>array(
			"models"=>"models",
			"controllers"=>"controllers",
			"rest"=>""
			),
	"isRest"=>function (){
			return \Ubiquity\utils\http\URequest::getUrlParts()[0]==="rest";
		}
	);