<?php
namespace models;
class Story{
	/**
	 * @id
	 * @column("name"=>"id","nullable"=>false,"dbType"=>"int(11)")
	*/
	private $id;

	/**
	 * @column("name"=>"name","nullable"=>true,"dbType"=>"varchar(45)")
	*/
	private $name;

	/**
	 * @manyToOne
	 * @joinColumn("className"=>"models\\Project","name"=>"idProject","nullable"=>false)
	*/
	private $project;

	/**
	 * @oneToMany("mappedBy"=>"story","className"=>"models\\Estimation")
	*/
	private $estimations;

	 public function getId(){
		return $this->id;
	}

	 public function setId($id){
		$this->id=$id;
	}

	 public function getName(){
		return $this->name;
	}

	 public function setName($name){
		$this->name=$name;
	}

	 public function getProject(){
		return $this->project;
	}

	 public function setProject($project){
		$this->project=$project;
	}

	 public function getEstimations(){
		return $this->estimations;
	}

	 public function setEstimations($estimations){
		$this->estimations=$estimations;
	}

	 public function __toString(){
		return $this->name;
	}

}