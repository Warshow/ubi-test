<?php
namespace models;
class Project{
	/**
	 * @id
	 * @column("name"=>"id","nullable"=>false,"dbType"=>"int(11)")
	*/
	private $id;

	/**
	 * @column("name"=>"name","nullable"=>true,"dbType"=>"varchar(45)")
	*/
	private $name;

	/**
	 * @column("name"=>"points","nullable"=>false,"dbType"=>"text")
	*/
	private $points;

	/**
	 * @oneToMany("mappedBy"=>"project","className"=>"models\\Story")
	*/
	private $storys;

	/**
	 * @manyToOne
	 * @joinColumn("className"=>"models\\User","name"=>"idUser","nullable"=>false)
	*/
	private $user;

	/**
	 * @manyToMany("targetEntity"=>"models\\User","inversedBy"=>"projects")
	 * @joinTable("name"=>"project_has_user")
	*/
	private $users;

	 public function getId(){
		return $this->id;
	}

	 public function setId($id){
		$this->id=$id;
	}

	 public function getName(){
		return $this->name;
	}

	 public function setName($name){
		$this->name=$name;
	}

	 public function getPoints(){
		return $this->points;
	}

	 public function setPoints($points){
		$this->points=$points;
	}

	 public function getStorys(){
		return $this->storys;
	}

	 public function setStorys($storys){
		$this->storys=$storys;
	}

	 public function getUser(){
		return $this->user;
	}

	 public function setUser($user){
		$this->user=$user;
	}

	 public function getUsers(){
		return $this->users;
	}

	 public function setUsers($users){
		$this->users=$users;
	}

	 public function __toString(){
		return $this->name;
	}

}