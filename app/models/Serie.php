<?php
namespace models;
class Serie{
	/**
	 * @id
	 * @column("name"=>"id","nullable"=>false,"dbType"=>"int(11)")
	*/
	private $id;

	/**
	 * @column("name"=>"name","nullable"=>true,"dbType"=>"varchar(45)")
	*/
	private $name;

	/**
	 * @column("name"=>"points","nullable"=>true,"dbType"=>"text")
	*/
	private $points;

	 public function getId(){
		return $this->id;
	}

	 public function setId($id){
		$this->id=$id;
	}

	 public function getName(){
		return $this->name;
	}

	 public function setName($name){
		$this->name=$name;
	}

	 public function getPoints(){
		return $this->points;
	}

	 public function setPoints($points){
		$this->points=$points;
	}

	 public function __toString(){
		return $this->id;
	}

}