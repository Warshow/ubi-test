<?php
namespace controllers;
 use Ubiquity\orm\DAO;
use models\Project;
use models\User;
use models\Story;
use Ajax\php\ubiquity\JsUtils;
use Ubiquity\utils\http\URequest;
use Ubiquity\utils\http\UResponse;
use Ajax\semantic\html\base\traits\IconTrait;

 /**
 * Controller ProjectsController
 * @property \Ajax\php\ubiquity\JsUtils $jquery
 **/
class ProjectsController extends ControllerBase{

	public function index(){
		$projects=DAO::getAll(Project::class);
		$this->loadView("ProjectsController/index.html",["projects"=>$projects]);
	}
	
	public function addNew($name) {
	    $user=DAO::getOne(User::class, 1);
	    $project=new Project();
	    $project->setName($name);
	    $project->setUser($user);
	    DAO::save($project);
	    $this->index();
	}
	
	/**
	 * @route("projects")
	 **/
	public function all($idProject=null) {
	    $projects=DAO::getAll(Project::class,'',false);
	    $stories="";
	    if(isset($idProject)){
	        $stories=$this->stories($idProject,true);
	    }
	    $this->jquery->getHref("a","",["ajaxTransition"=>"random","hasLoader"=>false]);
	    $this->jquery->renderView("ProjectsController/all.html",compact("projects","stories"));//crée un tableau associatif a partir des info en param
	}

	/**
	 *@get("project/stories/{idProject}","requirements"=>["idProject"=>"\d+"])
	**/
	public function stories($idProject,$asString=false){
	    if(URequest::isAjax() || $asString===true){
        $project=DAO::getOne(Project::class,$idProject,["storys"]);
        $this->jquery->getOn("mouseenter", "._stories", "project/story/","#divEstimations",["attr"=>"data-ajax","hasLoader"=>false]);
        $this->jquery->renderView('ProjectsController/stories.html',compact("project"),$asString);
	    }else{
	        $this->all($idProject);
	    }

	}
	
	/**
	 * @get("project/story/{idStory}","requirements"=>["idStory"=>"\d+"])
	 * @param int $idStory
	 */
	public function estimations($idStory) {
	    $story=DAO::getOne(Story::class,$idStory,["estimations.user"]);
	    $message=$this->jquery->semantic()->htmlMessage("msg");
	    //$list=$this->jquery->semantic()->htmlList("list-estimations",$story->getEstimation());
	    $message->addHeader($story->getName());
	    $message->addList($story->getEstimations());
	    $message->setIcon("bath loader");
	    $this->jquery->renderView("ProjectsController/estimations.html");
	}
	
	/**
	 * @get("project/estimations/{idProject}","requirements"=>["idStory"=>"\d+"])
	 */
	public function ChooseEstim($idProject) {
	    ;
	}

}
