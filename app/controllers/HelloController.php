<?php
namespace controllers;
 /**
 * Controller HelloController
 **/
class HelloController extends ControllerBase{

	public function index(){
		echo "Message  é";
	}
    
	/**
	 * @get("msg/{message}/","name"=>"message")
	 * @param string $message
	 **/
	public function show($message="Hello World"){
		echo $message;
	}


	/**
	 *@route("vue/{message}","methods"=>["post","get"])
	**/
	public function messageAvecVue($message){
		
	    $this->loadView('HelloController/messageAvecVue.html',["msg"=>$message]);

	}

}
